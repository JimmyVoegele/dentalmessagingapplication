﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data.SqlClient;
using System.Web.UI.WebControls;

using System.Configuration;
using System.Data;

using Telerik.Web.UI;
using IO.DialMyCalls.Model;

using System.Net.Mail;

namespace DentalMessagingApplication
{
    public partial class _Default : Page
    {
        DialMyCalls dmc = new DialMyCalls();

        string m_strDialMyCallsAPIKey = string.Empty;

        string m_strEndofScreeningK4CallID = string.Empty;
        string m_strEndofScreening5thGradeCallID = string.Empty;

        string m_strEndofTreatmentK4CallID = string.Empty;
        string m_strEndofTreatment5thGradeCallID = string.Empty;

        string m_strEndofScreeningReminderCallID = string.Empty;

        string m_strGeneralTextMessage = string.Empty;
        string m_strEndofTreatment5thGradeTextMessage = string.Empty;
        string m_strTextMessageKeyword = string.Empty;

        string m_strTextMessageTime = string.Empty;
        string m_strCallTime = string.Empty;

        static List<ContactAttributesEX> m_lstBadStudents = null;
        static List<ContactAttributesEX> m_lstDuplicateStudents = null;


        protected void Page_Load(object sender, EventArgs e)
        {
            LoadAPIKey();
            if (!IsPostBack)
            {
                UpdateCallStatus();
                LoadSchools();
                LoadGrids();
                txtAdminAPIKey.Text = dmc.GetAPIKey();
                LoadAdminScreen();
                lblEnvironment.Text = Environment.UserName;
                lblEnvironmentServer.Text = Request.ServerVariables["LOGON_USER"];
                ShowAdminTab();

            }
        }

        private void LoadGrids()
        {
            SqlDataSource1.SelectCommand = "SELECT TOP 20 CallID, DialMyCallsStatus, BroadCastName, CallTime, MessageType, RecordingName, TextContent, Students, SendTime, SentBy,  DebugMode FROM vwCallsHistory where DebugMode = 0 and DialMyCallAPIKey like '" + m_strDialMyCallsAPIKey + "%' ORDER BY CallTime DESC";
            RadGrid1.Rebind();
            int nSchoolID = 0;
            if (cmbSchools.SelectedItem != null)
            {
                nSchoolID = Convert.ToInt32(cmbSchools.SelectedItem.Value);
                SqlDataSource2.SelectCommand = "SELECT TOP 1000 school_name, student_first_name, student_last_name, teacher_name, form_type, not_screened_reason, consent_returned, student_phone, grade, PilotStudent  FROM vwEOSStudents where school_id = " + nSchoolID;
                RadGrid2.Rebind();

                lblStudentCount.Text = RadGrid2.MasterTableView.Items.Count.ToString();
                CalculateDuplicateNumbers();
            }

                nSchoolID = Convert.ToInt32(cmbSchoolsReminder.SelectedItem.Value);
                SqlDataSource3.SelectCommand = "SELECT TOP 1000 school_name, student_first_name, student_last_name, teacher_name, form_type, not_screened_reason, consent_returned, student_phone, grade  FROM vwEOSStudentsReminder where school_id = " + nSchoolID;
                RadGrid3.Rebind();
                CalculateDuplicateNumbersReminder();
                lblStudentCountReminder.Text = RadGrid3.MasterTableView.Items.Count.ToString();

                SqlDataSource4.SelectCommand = "SELECT TOP 1000 school_name, student_last_name, student_first_name, teacher_name, form_type, not_screened_reason, consent_returned, student_phone, grade, PilotStudent   FROM vwEOTStudentsQueue Order by school_name, student_last_name, student_first_name ";
                RadGrid4.Rebind();
                CalculateDuplicateNumbersEOT();
                lblEOTStudentCount.Text = RadGrid4.MasterTableView.Items.Count.ToString();
            

        }

        private void ShowAdminTab()
        {
            string selectSQL = "Select EnvironmentName from [db_owner].[AdminUsers]";
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);
            SqlDataReader reader;

            string stEnvironmentName = Request.ServerVariables["LOGON_USER"];
            bool bAdminUser = false;


            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    string strName = reader["EnvironmentName"].ToString();
                    if (strName.ToLower().Trim() == stEnvironmentName.ToLower().Trim())
                    {
                        bAdminUser = true;
                        break;
                    }

                    strName = Request.ServerVariables["LOGON_USER"];
                    if (strName.ToLower().Trim() == stEnvironmentName.ToLower().Trim())
                    {
                        bAdminUser = true;
                        break;
                    }

                }
                reader.Close();
            }
            catch (Exception err)
            {
                //Response.Redirect("~/Error.aspx?Error=" + "Error Loading Focus Areas: " + err.Message);
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }

            RadTab rootTabUser = RadTabStrip1.FindTabByText("Administration");
            rootTabUser.Visible = bAdminUser;
        }


        private void UpdateCallStatus()
        {
            ResetCallStatus();

            string selectSQL = "Select ID, MessageType, DialMyCallGUID from [db_owner].[CallsHistory] where DialMyCallsStatus = 'queued' and DialMyCallAPIKey like '" + m_strDialMyCallsAPIKey + "%'";

            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);
            SqlDataReader reader;

            string strGUID = string.Empty;
            string strCallID = string.Empty;
            string strMessageType = string.Empty;
            DataTable dtCalls = null;

            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    strMessageType = reader["MessageType"].ToString();
                    strGUID = reader["DialMyCallGUID"].ToString();
                    strCallID = reader["ID"].ToString();
                    try
                    {
                        if (strMessageType.ToUpper() == "TEXT")
                        {
                            dtCalls = dmc.GetTextStatus(strGUID);
                        }
                        else
                        {
                            dtCalls = dmc.GetCallStatus(strGUID);
                        }
                        UpdateHistoryStatus(strCallID, strGUID, dtCalls);
                    } catch
                    {
                        //Leave this blank and do the next one
                    }

                }
                reader.Close();
            }
            catch (Exception err)
            {
                //Response.Redirect("~/Error.aspx?Error=" + "Error Loading Focus Areas: " + err.Message);
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }

            SqlDataSource1.SelectCommand = "SELECT TOP 20 CallID, DialMyCallsStatus, BroadCastName, CallTime, MessageType, RecordingName, TextContent, Students, SendTime, SentBy,  DebugMode FROM vwCallsHistory where DebugMode = 0 and DialMyCallAPIKey like '" + m_strDialMyCallsAPIKey + "%' ORDER BY CallTime DESC";
            RadGrid1.Rebind();
        }

        private void ResetCallStatus()
        {
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand("[dbo].[ResetDialMyCallsHistory]", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@APIKey", dmc.GetAPIKey());

            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception err)
            {
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }
        }

        private void UpdateHistoryStatus(string strCallID, string strGUID, DataTable dt)
        {
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            string strOverallStatus = "Success";

            string strFirstName = string.Empty;
            string strLastName = string.Empty;
            string strPhoneNumber = string.Empty;
            string strGrade = string.Empty;

            string strStatus = string.Empty;
            string strDuration = string.Empty;
            string strAttempts = string.Empty;
            string strSuccessful = string.Empty;
            string strCalledAt = string.Empty;

            foreach (DataRow dr in dt.Rows)
            {
                strFirstName = dr["FirstName"].ToString();
                strLastName = dr["Lastname"].ToString();
                strPhoneNumber = dr["Phone"].ToString();
                strGrade = dr["Grade"].ToString();
                strStatus = dr["Status"].ToString();
                strDuration = dr["Duration"].ToString();
                strAttempts = dr["Attempts"].ToString();
                strSuccessful = dr["Successful"].ToString();
                strCalledAt = dr["CalledAt"].ToString();

                if (strStatus != "queued" && strStatus != "")
                {

                    string InsertSQL = "Insert into [db_owner].[DialMyCallsHistory] (CallID, FirstName, LastName, PhoneNumber, Grade, Status, Duration, Attempts, Successful, CalledAt) values (" + Convert.ToUInt32(strCallID) + ",'" + strFirstName.Replace("'", "''") + "','" + strLastName.Replace("'", "''") + "','" + strPhoneNumber + "','" + strGrade + "','" + strStatus + "','" + strDuration + "','" + strAttempts + "','" + strSuccessful + "','" + strCalledAt + "')";

                    string connStringInsert = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

                    SqlConnection connInsert = new SqlConnection(connString);
                    SqlCommand cmdInsert = new SqlCommand(InsertSQL, connInsert);

                    try
                    {
                        connInsert.Open();
                        cmdInsert.ExecuteScalar();
                    }
                    catch (Exception err)
                    {
                        //Response.Redirect("~/Error.aspx?Error=" + "Error Loading Focus Areas: " + err.Message);
                        Console.Write(err);
                    }
                    finally
                    {
                        connInsert.Close();
                    }


                    if (strSuccessful != "true")
                    {
                        strOverallStatus = "Failed";
                    }
                } else
                {
                    strOverallStatus = "queued";
                }

            }

            if (strOverallStatus != "queued")
            {
                string selectSQL = "Update [db_owner].[CallsHistory] SET DialMyCallsStatus = '" + strOverallStatus + "' where DialMyCallGUID = '" + strGUID + "'";

                SqlConnection conn = new SqlConnection(connString);
                SqlCommand cmd = new SqlCommand(selectSQL, conn);

                try
                {
                    conn.Open();
                    cmd.ExecuteScalar();
                }
                catch (Exception err)
                {
                    //Response.Redirect("~/Error.aspx?Error=" + "Error Loading Focus Areas: " + err.Message);
                    Console.Write(err);
                }
                finally
                {
                    conn.Close();
                }

            }
        }

        private void LoadAPIKey()
        {
            string selectSQL = "Select DialMyCallsAPIKey, EndofScreeningK4CallID,  EndofScreening5thGradeCallID, EndofScreeningReminderCallID, EndofTreatmentK4CallID, EndofTreatment5thGradeCallID, TextMessageKeyword, GeneralTextMessage, EndofTreatment5thGradeTextMessage, TextMessageTime, CallTime, DebugMode from AdminPreferences";

            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);
            SqlDataReader reader;

            string strValue = string.Empty;

            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    strValue = reader["DialMyCallsAPIKey"].ToString();
                    m_strDialMyCallsAPIKey = strValue;
                    dmc.LoadAPIKey(strValue);

                    var recordings = dmc.GetRecordings();

                    //strValue = reader["DialMyCallsCallerID"].ToString();
                    //dmc.SetCallerID(strValue);

                    strValue = reader["EndofScreeningK4CallID"].ToString();
                    m_strEndofScreeningK4CallID = strValue;


                    lblEOSK4.Text = dmc.GetRecordingName(m_strEndofScreeningK4CallID);

                    strValue = reader["EndofScreening5thGradeCallID"].ToString();
                    m_strEndofScreening5thGradeCallID = strValue;


                    lblEOS5TH.Text = dmc.GetRecordingName(m_strEndofScreening5thGradeCallID);

                    strValue = reader["EndofScreeningReminderCallID"].ToString();
                    m_strEndofScreeningReminderCallID = strValue;

                    lblEOSReminder.Text = dmc.GetRecordingName(m_strEndofScreeningReminderCallID);

                    strValue = reader["EndofTreatmentK4CallID"].ToString();
                    m_strEndofTreatmentK4CallID = strValue;
                    lblEOTK4.Text = dmc.GetRecordingName(m_strEndofTreatmentK4CallID);

                    strValue = reader["EndofTreatment5thGradeCallID"].ToString();
                    m_strEndofTreatment5thGradeCallID = strValue;
                    lblEOT5TH.Text = dmc.GetRecordingName(m_strEndofTreatment5thGradeCallID);

                    strValue = reader["TextMessageKeyword"].ToString();
                    m_strTextMessageKeyword = strValue;

                    strValue = reader["GeneralTextMessage"].ToString();
                    m_strGeneralTextMessage = strValue;
                    lblEOSTMK4.Text = m_strGeneralTextMessage;
                    lblEOTTMK4.Text = m_strGeneralTextMessage;
                    lblEOSTMReminder.Text = m_strGeneralTextMessage;

                    strValue = reader["EndofTreatment5thGradeTextMessage"].ToString();
                    m_strEndofTreatment5thGradeTextMessage = strValue;
                    lblEOSTM5TH.Text = m_strEndofTreatment5thGradeTextMessage;
                    lblEOTTM5TH.Text = m_strEndofTreatment5thGradeTextMessage;

                }
                reader.Close();
            }
            catch (Exception err)
            {
                //Response.Redirect("~/Error.aspx?Error=" + "Error Loading Focus Areas: " + err.Message);
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }

        }

        private void LoadSchools()
        {
            string selectSQL = "Select id, name, EOSCOUNT, EOSQueue, EOSReminderCOUNT, EOSReminderQueue from vwSchools order by name";

            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);
            SqlDataReader reader;

            string strSchoolName = string.Empty;
            string strSchoolID = string.Empty;
            Dictionary<string, string> dctSchool = new Dictionary<string, string>();

            cmbSchools.Items.Clear();
            cmbSchoolsQueued.Items.Clear();
            cmbSchoolsReminder.Items.Clear();
            cmbSchoolsReminderQueued.Items.Clear();

            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {

                    strSchoolID = reader["id"].ToString();
                    strSchoolName = reader["name"].ToString();

                    string nEOSCount = reader["EOSCOUNT"].ToString();
                    string nEOSQueueCount = reader["EOSQueue"].ToString();

                    RadComboBoxItem rcbItem = new RadComboBoxItem(strSchoolName + " (" + nEOSCount + ")", strSchoolID);
                    if (Convert.ToInt32(nEOSCount) > 0)
                    {
                        if (Convert.ToInt32(nEOSQueueCount) == 0)
                            cmbSchools.Items.Add(rcbItem);
                        else
                            cmbSchoolsQueued.Items.Add(rcbItem);
                    }

                    string nEOSReminderCount = reader["EOSReminderCOUNT"].ToString();
                    string nEOSReminderQueueCount = reader["EOSReminderQueue"].ToString();

                    RadComboBoxItem rcbItemReminder = new RadComboBoxItem(strSchoolName + " (" + nEOSReminderCount + ")", strSchoolID);

                    if (Convert.ToInt32(nEOSReminderCount) > 0)
                    {
                        if (Convert.ToInt32(nEOSReminderQueueCount) == 0)
                            cmbSchoolsReminder.Items.Add(rcbItemReminder);
                        else
                            cmbSchoolsReminderQueued.Items.Add(rcbItemReminder);
                    }
                }
                reader.Close();
                cmbSchools.SelectedIndex = 0;
                cmbSchoolsReminder.SelectedIndex = 0;
            }
            catch (Exception err)
            {
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }
            ReloadEOSGridItems();
            ReloadEOSReminderGridItems();
        }

        private List<ContactAttributesEX> CreateStudents(string strGroupID, string strSelectSQL)
        {
            List<string> strGroup = new List<string>();
            strGroup.Add(strGroupID);

            //Query to Get Students
            List<ContactAttributesEX> lstStudents = new List<ContactAttributesEX>();

            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(strSelectSQL, conn);
            SqlDataReader reader;

            string strStudentFirstName = string.Empty;
            string strStudentLastName = string.Empty;
            string strStudentPhone = string.Empty;
            string strStudentGrade = string.Empty;
            int nStudentSchoolID = 0;
            bool bIsPilotStudent = false;

            Dictionary<string, string> dctSchool = new Dictionary<string, string>();

            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    strStudentFirstName = reader["student_first_name"].ToString();
                    strStudentLastName = reader["student_last_name"].ToString();
                    strStudentPhone = reader["student_phone"].ToString();
                    strStudentGrade = reader["Grade"].ToString();
                    nStudentSchoolID = Convert.ToInt32(reader["school_id"].ToString());
                    bIsPilotStudent = Convert.ToBoolean(reader["PilotStudent"].ToString());
                    ContactAttributesEX CA = new ContactAttributesEX(strStudentPhone, strStudentFirstName, strStudentLastName, strStudentGrade, nStudentSchoolID, bIsPilotStudent);
                    lstStudents.Add(CA);

                }
                reader.Close();
            }
            catch (Exception err)
            {
                //Response.Redirect("~/Error.aspx?Error=" + "Error Loading Focus Areas: " + err.Message);
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }

            Hashtable htNumbers = new Hashtable();

            m_lstBadStudents = new List<ContactAttributesEX>();
            m_lstDuplicateStudents = new List<ContactAttributesEX>();
            foreach (ContactAttributesEX caStudent in lstStudents)
            {
                    if (!htNumbers.Contains(caStudent.CA.Phone))
                    {
                        htNumbers.Add(caStudent.CA.Phone, caStudent.CA.Phone);
                        if (!dmc.CreateContact(caStudent.CA.Firstname, caStudent.CA.Lastname, caStudent.CA.Phone, "", caStudent.CA.Email, strGroup))
                        {
                        m_lstBadStudents.Add(caStudent);
                        }
                    }
                    else
                    {
                    m_lstDuplicateStudents.Add(caStudent);
                        Console.Write("Duplicate");
                    }
            }

            if (m_lstBadStudents.Count > 0)
            {
                foreach (ContactAttributesEX caStudent in m_lstBadStudents)
                {
                    lstStudents.Remove(caStudent);
                }
            }

            return lstStudents;
        }

        private string CreateSchoolGroup(string strGroupName)
        {
            return dmc.CreateGroup(strGroupName);
        }


        protected void cmbSchools_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            ReloadEOSGridItems();
        }

        private void ReloadEOSGridItems()
        {if (cmbSchools.SelectedItem != null)
            {
                int nSchoolID = Convert.ToInt32(cmbSchools.SelectedItem.Value);
                SqlDataSource2.SelectCommand = "SELECT TOP 1000 school_name, student_first_name, student_last_name, teacher_name, form_type, not_screened_reason, consent_returned, student_phone, grade, PilotStudent  FROM vwEOSStudents where school_id = " + nSchoolID.ToString();
                RadGrid2.Rebind();
                lblStudentCount.Text = RadGrid2.MasterTableView.Items.Count.ToString();
                CalculateDuplicateNumbers();
            }
        }

        protected void cmbSchoolsReminder_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            ReloadEOSReminderGridItems();
        }

        private void ReloadEOSReminderGridItems()
        {
            int nSchoolID = Convert.ToInt32(cmbSchoolsReminder.SelectedItem.Value);
            SqlDataSource3.SelectCommand = "SELECT TOP 1000 school_name, student_first_name, student_last_name, teacher_name, form_type, not_screened_reason, consent_returned, student_phone, grade  FROM vwEOSStudentsReminder where school_id = " + nSchoolID.ToString();
            RadGrid3.Rebind();
            CalculateDuplicateNumbersReminder();
            lblStudentCountReminder.Text = RadGrid3.MasterTableView.Items.Count.ToString();
        }

        protected void btnEOS_Click(object sender, EventArgs e)
        {
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand("[dbo].[AddEOSSchoolToQueue]", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            int nSchoolID = Convert.ToInt32(cmbSchools.SelectedItem.Value);
            cmd.Parameters.AddWithValue("@SchoolID", cmbSchools.SelectedValue);

            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception err)
            {
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }

            LoadSchools();
        }

        protected void btnEOSReminder_Click(object sender, EventArgs e)
        {
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand("[dbo].[AddEOSReminderSchoolToQueue]", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            int nSchoolID = Convert.ToInt32(cmbSchoolsReminder.SelectedItem.Value);
            cmd.Parameters.AddWithValue("@SchoolID", nSchoolID);

            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception err)
            {
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }

            LoadSchools();
        }


        protected void RadGrid1_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            UpdateCallStatus();
        }

        protected void btnAdminLoadAPIKey_Click(object sender, EventArgs e)
        {
            LoadAdminScreen();
        }

        private void LoadAdminScreen()
        {
            //DialMyCalls dmcNew = new DialMyCalls();

            //dmcNew.LoadAPIKey(txtAdminAPIKey.Text);

            string strAdminEOSCall = dmc.GetRecordingName(m_strEndofScreeningK4CallID);
            string strAdminEOS5thGradeCall = dmc.GetRecordingName(m_strEndofScreening5thGradeCallID);
            string strAdminEOSReminderCall = dmc.GetRecordingName(m_strEndofScreeningReminderCallID);
            string strAdminEOTCall = dmc.GetRecordingName(m_strEndofTreatmentK4CallID);
            string strAdminEOT5thGradeCall = dmc.GetRecordingName(m_strEndofTreatment5thGradeCallID);

            Dictionary<string, string> Recordings = dmc.GetRecordings();

            cmbAdminEOSCall.Items.Clear();
            cmbAdminEOS5thGradeCall.Items.Clear();
            cmbAdminEOSReminderCall.Items.Clear();
            cmbAdminEOTCall.Items.Clear();
            cmbAdminEOT5thGradeCall.Items.Clear();


            foreach (KeyValuePair<string, string> result in Recordings)
            {
                RadComboBoxItem item = new RadComboBoxItem(result.Value, result.Key);
                cmbAdminEOSCall.Items.Add(item);
                item = new RadComboBoxItem(result.Value, result.Key);
                cmbAdminEOS5thGradeCall.Items.Add(item);
                item = new RadComboBoxItem(result.Value, result.Key);
                cmbAdminEOSReminderCall.Items.Add(item);
                item = new RadComboBoxItem(result.Value, result.Key);
                cmbAdminEOTCall.Items.Add(item);
                item = new RadComboBoxItem(result.Value, result.Key);
                cmbAdminEOT5thGradeCall.Items.Add(item);
            }

            foreach (RadComboBoxItem item in cmbAdminEOSCall.Items)
            {
                if (item.Text == strAdminEOSCall)
                    cmbAdminEOSCall.SelectedIndex = item.Index;
            }

            foreach (RadComboBoxItem item in cmbAdminEOS5thGradeCall.Items)
            {
                if (item.Text == strAdminEOS5thGradeCall)
                    cmbAdminEOS5thGradeCall.SelectedIndex = item.Index;
            }

            foreach (RadComboBoxItem item in cmbAdminEOSReminderCall.Items)
            {
                if (item.Text == strAdminEOSReminderCall)
                    cmbAdminEOSReminderCall.SelectedIndex = item.Index;
            }

            foreach (RadComboBoxItem item in cmbAdminEOTCall.Items)
            {
                if (item.Text == strAdminEOTCall)
                    cmbAdminEOTCall.SelectedIndex = item.Index;
            }

            foreach (RadComboBoxItem item in cmbAdminEOT5thGradeCall.Items)
            {
                if (item.Text == strAdminEOT5thGradeCall)
                    cmbAdminEOT5thGradeCall.SelectedIndex = item.Index;
            }

            txtAdminGeneralTM.Text = m_strGeneralTextMessage;
            txtAdmin5thGradeTM.Text = m_strEndofTreatment5thGradeTextMessage;

            Dictionary<string, string> m_strTextMessageKeyword = dmc.GetTextKeyWords();

            cmbTextKeywords.Items.Clear();
            foreach (KeyValuePair<string, string> result in m_strTextMessageKeyword)
            {
                RadComboBoxItem item = new RadComboBoxItem(result.Value, result.Key);
                cmbTextKeywords.Items.Add(item);
            }

            dmc.LoadAPIKey(m_strDialMyCallsAPIKey);
        }

        protected void btnAdminSave_Click(object sender, EventArgs e)
        {
            string selectSQL = "Update [db_owner].[AdminPreferences] SET DialMyCallsAPIKey = '" + txtAdminAPIKey.Text + "', EndofScreeningK4CallID = '" + cmbAdminEOSCall.SelectedValue + "', EndofScreening5thGradeCallID = '" + cmbAdminEOS5thGradeCall.SelectedValue + "', EndofScreeningReminderCallID = '" + cmbAdminEOSReminderCall.SelectedValue + "', EndofTreatmentK4CallID = '" + cmbAdminEOTCall.SelectedValue + "', EndofTreatment5thGradeCallID = '" + cmbAdminEOT5thGradeCall.SelectedValue + "', TextMessageKeyword = '" + cmbTextKeywords.SelectedValue + "', GeneralTextMessage= '" + txtAdminGeneralTM.Text.Replace("'", "''") + "', EndofTreatment5thGradeTextMessage='" + txtAdmin5thGradeTM.Text.Replace("'", "''") + "'";

            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);

            try
            {
                conn.Open();
                cmd.ExecuteScalar();
            }
            catch (Exception err)
            {
                //Response.Redirect("~/Error.aspx?Error=" + "Error Loading Focus Areas: " + err.Message);
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }

            LoadAPIKey();
            LoadSchools();
            LoadAdminScreen();
            txtAdminAPIKey.Text = dmc.GetAPIKey();
            UpdateCallStatus();
            RadGrid1.Rebind();

        }

        protected void RadGrid1_ItemCommand(object sender, GridCommandEventArgs e)
        {
            UpdateCallStatus();
            RadGrid1.ExportSettings.Excel.Format = Telerik.Web.UI.GridExcelExportFormat.Biff;
            RadGrid1.ExportSettings.IgnorePaging = true;
            RadGrid1.ExportSettings.ExportOnlyData = true;
            RadGrid1.ExportSettings.OpenInNewWindow = true;
        }

        protected void RadGrid2_ItemCommand(object sender, GridCommandEventArgs e)
        {
            int nSchoolID = Convert.ToInt32(cmbSchools.SelectedItem.Value);
            SqlDataSource2.SelectCommand = "SELECT TOP 1000 school_name, student_first_name, student_last_name, teacher_name, form_type, not_screened_reason, consent_returned, student_phone, grade, PilotStudent  FROM vwEOSStudents where school_id = " + nSchoolID.ToString();
            RadGrid2.Rebind();
            CalculateDuplicateNumbers();

            RadGrid2.ExportSettings.Excel.Format = Telerik.Web.UI.GridExcelExportFormat.Biff;
            RadGrid2.ExportSettings.IgnorePaging = true;
            RadGrid2.ExportSettings.ExportOnlyData = true;
            RadGrid2.ExportSettings.OpenInNewWindow = true;
        }

        protected void RadGrid3_ItemCommand(object sender, GridCommandEventArgs e)
        {
            int nSchoolID = Convert.ToInt32(cmbSchoolsReminder.SelectedItem.Value);

            SqlDataSource3.SelectCommand = "SELECT TOP 1000 school_name, student_first_name, student_last_name, teacher_name, form_type, not_screened_reason, consent_returned, student_phone, grade  FROM vwEOSStudentsReminder where school_id = " + nSchoolID.ToString();
            RadGrid3.Rebind();
            CalculateDuplicateNumbersReminder();
            RadGrid3.ExportSettings.Excel.Format = Telerik.Web.UI.GridExcelExportFormat.Biff;
            RadGrid3.ExportSettings.IgnorePaging = true;
            RadGrid3.ExportSettings.ExportOnlyData = true;
            RadGrid3.ExportSettings.OpenInNewWindow = true;
        }

        protected void RadGrid4_ItemCommand(object sender, GridCommandEventArgs e)
        {
            SqlDataSource4.SelectCommand = "SELECT TOP 1000 school_name, student_last_name, student_first_name, teacher_name, form_type, not_screened_reason, consent_returned, student_phone, grade, PilotStudent FROM vwEOTStudentsQueue Order by school_name, student_last_name, student_first_name ";
            RadGrid4.Rebind();
            CalculateDuplicateNumbersEOT();
            RadGrid4.ExportSettings.Excel.Format = Telerik.Web.UI.GridExcelExportFormat.Biff;
            RadGrid4.ExportSettings.IgnorePaging = true;
            RadGrid4.ExportSettings.ExportOnlyData = true;
            RadGrid4.ExportSettings.OpenInNewWindow = true;
        }

        protected void CalculateDuplicateNumbers()
        {
            try
            {
                List<string> lstNonPilot = new List<string>();
                List<string> lstPilotK4 = new List<string>();
                List<string> lstPilot5th = new List<string>();

                foreach (GridDataItem row in RadGrid2.MasterTableView.Items)
                {
                    string strPilotStudent = row["pilotstudent"].Text;
                    string strPhoneNumber = row["student_phone"].Text;
                    string strGrade = row["grade"].Text;
                    if (strPilotStudent.Trim().ToLower() == "true")
                    {
                        if (strGrade == "5")
                        {
                            lstPilot5th.Add(strPhoneNumber);
                        }
                        else
                        {
                            lstPilotK4.Add(strPhoneNumber);
                        }

                    }
                    else
                    {
                        lstNonPilot.Add(strPhoneNumber);
                    }

                }

                Hashtable htNumbers = new Hashtable();
                int nDuplicates = 0;
                foreach (string str in lstNonPilot)
                {
                    if (htNumbers.Contains(str))
                    {
                        nDuplicates++;
                    }
                    else
                    {
                        htNumbers.Add(str, str);
                    }
                }

                htNumbers = new Hashtable();
                foreach (string str in lstPilotK4)
                {
                    if (str == "5123852647")
                        Console.Write("123");

                    if (htNumbers.Contains(str))
                    {
                        nDuplicates++;
                    }
                    else
                    {
                        htNumbers.Add(str, str);
                    }
                }

                htNumbers = new Hashtable();
                foreach (string str in lstPilot5th)
                {
                    if (htNumbers.Contains(str))
                    {
                        nDuplicates++;
                    }
                    else
                    {
                        htNumbers.Add(str, str);
                    }
                }

                lblDuplicateNumbers.Text = nDuplicates.ToString();
                int nTotal = RadGrid2.MasterTableView.Items.Count - nDuplicates;
                lblDialMyCalls.Text = nTotal.ToString();
            }
            catch (Exception EX)
            {
                lblDuplicateNumbers.Text = EX.Message;
            }

        }

        protected void CalculateDuplicateNumbersReminder()
        {
            try
            {
                List<string> lstNonPilot = new List<string>();

                foreach (GridDataItem row in RadGrid3.MasterTableView.Items)
                {
                    string strPhoneNumber = row["student_phone"].Text;
                    lstNonPilot.Add(strPhoneNumber);
                }

                Hashtable htNumbers = new Hashtable();
                int nDuplicates = 0;
                foreach (string str in lstNonPilot)
                {
                    if (htNumbers.Contains(str))
                    {
                        nDuplicates++;
                    }
                    else
                    {
                        htNumbers.Add(str, str);
                    }
                }


                lblDuplicateNumbersReminder.Text = nDuplicates.ToString();
                int nTotal = RadGrid3.MasterTableView.Items.Count - nDuplicates;
                lblDialMyCallsReminder.Text = nTotal.ToString();
            }
            catch (Exception EX)
            {
                lblDuplicateNumbersReminder.Text = EX.Message;
            }

        }


        protected void CalculateDuplicateNumbersEOT()
        {
            try
            {
                List<string> lstNonPilot = new List<string>();
                List<string> lstPilotK4 = new List<string>();
                List<string> lstPilot5th = new List<string>();

                foreach (GridDataItem row in RadGrid4.MasterTableView.Items)
                {
                    string strPilotStudent = row["pilotstudent"].Text;
                    string strPhoneNumber = row["student_phone"].Text;
                    string strGrade = row["grade"].Text;
                    if (strPilotStudent.Trim().ToLower() == "true")
                    {
                        if (strGrade == "5")
                        {
                            lstPilot5th.Add(strPhoneNumber);
                        }
                        else
                        {
                            lstPilotK4.Add(strPhoneNumber);
                        }

                    }
                    else
                    {
                        lstNonPilot.Add(strPhoneNumber);
                    }

                }

                Hashtable htNumbers = new Hashtable();
                int nDuplicates = 0;
                foreach (string str in lstNonPilot)
                {
                    if (htNumbers.Contains(str))
                    {
                        nDuplicates++;
                    }
                    else
                    {
                        htNumbers.Add(str, str);
                    }
                }

                htNumbers = new Hashtable();
                foreach (string str in lstPilotK4)
                {
                    if (htNumbers.Contains(str))
                    {
                        nDuplicates++;
                    }
                    else
                    {
                        htNumbers.Add(str, str);
                    }
                }

                htNumbers = new Hashtable();
                foreach (string str in lstPilot5th)
                {
                    if (htNumbers.Contains(str))
                    {
                        nDuplicates++;
                    }
                    else
                    {
                        htNumbers.Add(str, str);
                    }
                }

                lblDuplicateNumbersEOT.Text = nDuplicates.ToString();
                int nTotal = RadGrid4.MasterTableView.Items.Count - nDuplicates;
                lblDialMyCallsEOT.Text = nTotal.ToString();
            }
            catch (Exception EX)
            {
                lblDuplicateNumbersEOT.Text = EX.Message;
            }

        }

        static void SendStatus(string strMessageSubject, int nK4Count, int n5thGradeCount, int nDuplicateStudents, List<ContactAttributesEX> lstBadStudents)
        {


            string strHead = "";
            string strTail = "";

            String userName = ConfigurationManager.AppSettings["MailUsername"].ToString();
            String password = ConfigurationManager.AppSettings["MailPassword"].ToString();


            MailMessage mail = new MailMessage();
            MailAddress from = new MailAddress(userName);

            mail.From = from;

            AddEmailAddresses(mail, "SummaryEmailAddresses");

            mail.Subject = strMessageSubject + DateTime.Now.ToString("MM/dd/yyyy");
            string strBody = "Below is the summary for " + DateTime.Now.ToString("MM/dd/yyyy");
            strBody = strBody + "<p>* - K-4th grade Student Count = " + nK4Count.ToString();
            strBody = strBody + "<p>* - 5th grade Student Count = " + n5thGradeCount.ToString();
            strBody = strBody + "<p>* - Duplicate Count = " + nDuplicateStudents.ToString();
            int nTotalCalls = nK4Count + n5thGradeCount - nDuplicateStudents;
            strBody = strBody + "<p>* - Total DMC Count = " + nTotalCalls.ToString();
            
            if (lstBadStudents.Count > 0)
            {
                strBody = strBody + "<p>* - Total Bad Number Count = " + lstBadStudents.Count.ToString();

                strBody = strBody + "<p>Bad Phone Numbers: ";
                foreach (ContactAttributesEX strBadStudent in lstBadStudents)
                {
                    strBody = strBody + "<p> Number: " + strBadStudent.CA.Phone;
                }
            }


            mail.IsBodyHtml = true;
            mail.Body = strHead + strBody + strTail;

            SmtpClient client = new SmtpClient();
            client.Host = "smtp.office365.com";
            client.Credentials = new System.Net.NetworkCredential(userName, password);
            client.Port = 587;
            client.EnableSsl = true;
            client.Send(mail);
        }

        static void AddEmailAddresses(MailMessage mail, string strSection)
        {
            String strSummaryEmailAddresses = ConfigurationManager.AppSettings[strSection].ToString();

            string[] strAddresses = strSummaryEmailAddresses.Split(';');
            foreach (string strTo in strAddresses)
            {
                MailAddress to = new MailAddress(strTo);
                mail.To.Add(to);
            }
        }

        protected void btnBadNumber_Click(object sender, EventArgs e)
        {
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand("AddBadNumber", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@BadNumber", txtBadNumber.Text);

            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception err)
            {
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }

        }
    }
}