﻿<%@ Page Title="Dental Messaging Application" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="DentalMessagingApplication._Default" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel1"></telerik:RadAjaxLoadingPanel>
    <telerik:RadFormDecorator ID="RadFormDecorator1" runat="server" DecorationZoneID="demo" DecoratedControls="All" EnableRoundedCorners="false" />
    <h2 style="color:#0091ba">Dental Messaging Application</h2>
    <br />
    <telerik:RadLabel Text="Environment" runat="server" Skin="Office2007"></telerik:RadLabel>
    <telerik:RadLabel ID="lblEnvironment" Text="" runat="server" Skin="Office2007" Font-Bold="true"></telerik:RadLabel>
    <br />
    <telerik:RadLabel Text="Environment Server" runat="server" Skin="Office2007"></telerik:RadLabel>
    <telerik:RadLabel ID="lblEnvironmentServer" Text="" runat="server" Skin="Office2007" Font-Bold="true"></telerik:RadLabel>
    <br />
    <br />
    <br />
    <telerik:RadTabStrip RenderMode="Lightweight" runat="server" ID="RadTabStrip1"  MultiPageID="RadMultiPage1" SelectedIndex="0" Skin="Office2007">
        <Tabs>
            <telerik:RadTab Text="History" Width="200px" Selected="True"></telerik:RadTab>
            <telerik:RadTab Text="End of Screening" Width="200px"></telerik:RadTab>
            <telerik:RadTab Text="End of Screening Reminder" Width="250px"></telerik:RadTab>
            <telerik:RadTab Text="End of Treament" Width="200px"></telerik:RadTab>
            <telerik:RadTab Text="Administration" Width="200px"></telerik:RadTab>

        </Tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage runat="server" ID="RadMultiPage1"  SelectedIndex="0">
        <telerik:RadPageView runat="server" ID="RadPageView1">
            <telerik:RadGrid ID="RadGrid1" UseSubmitBehavior="False"  AllowAutomaticInserts="false" DataSourceID="SqlDataSource1" runat="server" ClientSettings-Resizing-AllowColumnResize="true" Skin="Office2007" OnNeedDataSource="RadGrid1_NeedDataSource" OnItemCommand="RadGrid1_ItemCommand">
                <GroupingSettings CollapseAllTooltip="Collapse all groups" />
                <ClientSettings>
                    <Resizing AllowColumnResize="True" />
                </ClientSettings>
                <MasterTableView AutoGenerateColumns="False" CommandItemDisplay="Top" AllowAutomaticInserts="false">
                    <CommandItemSettings ShowExportToExcelButton="true" ShowRefreshButton="true" ShowCancelChangesButton="false" ShowSaveChangesButton="false" ShowAddNewRecordButton="false" />
                    <Columns>
                        <telerik:GridHyperLinkColumn HeaderStyle-Width="40px" ItemStyle-ForeColor="Blue" ItemStyle-Font-Underline="true" DataTextFormatString="{0}" Target="_blank" DataNavigateUrlFields="DialMyCallsStatus,CallID,MessageType,RecordingName, TextContent, Students" DataNavigateUrlFormatString="~/History.aspx?ID={1}&MessageType={2}&RecordingName={3}&TextContent={4}&Students={5}" DataTextField="DialMyCallsStatus" HeaderText="Status" SortExpression="DialMyCallsStatus" UniqueName="DialMyCallsStatus" FilterControlWidth="40px"></telerik:GridHyperLinkColumn>
                        <telerik:GridBoundColumn DataField="BroadCastName" HeaderText="Broadcast Name" HeaderStyle-Width="200px" ReadOnly="true" ></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CallTime" HeaderText="Call Time" HeaderStyle-Width="100px" ReadOnly="true" ></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="MessageType" HeaderText="Type" HeaderStyle-Width="40px" ReadOnly="true" ></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="RecordingName" HeaderText="Call Name" HeaderStyle-Width="250px" ReadOnly="true" ></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="TextContent" HeaderText="Message" HeaderStyle-Width="500px" ReadOnly="true" ></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Students" HeaderText="Students" HeaderStyle-Width="40px" ReadOnly="true" ></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="SendTime" HeaderText="Sent Time" HeaderStyle-Width="100px" ReadOnly="true" ></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="SentBy" HeaderText="Sent By" HeaderStyle-Width="40px" ReadOnly="true" ></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="DebugMode" HeaderText="Debug" HeaderStyle-Width="40px" ReadOnly="true" ></telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>

            </telerik:RadGrid>

         </telerik:RadPageView>
        <telerik:RadPageView runat="server" ID="RadPageView2">
            <br />
            <telerik:RadLabel ID="lblSchools" Text="Schools:" runat="server" Skin="Office2007" Font-Bold="true"></telerik:RadLabel>
            <telerik:RadComboBox ID="cmbSchools" AutoPostBack ="true" runat="server" style='padding-left:50px;padding-right:40px' Width="380px" Text="Allison Elem" OnSelectedIndexChanged="cmbSchools_SelectedIndexChanged"></telerik:RadComboBox>
            <telerik:RadLabel ID="lblSchoolsQueued" Text="Queued Schools:" runat="server" Skin="Office2007" Font-Bold="true"></telerik:RadLabel>
            <telerik:RadComboBox ID="cmbSchoolsQueued" AutoPostBack ="true" runat="server" style='padding-left:50px;padding-right:40px' Width="380px" Text=""></telerik:RadComboBox>
            <telerik:RadLabel ID="RadLabel2" Text=" ** If you do not see the school you are looking for please contact Todd Waldron or Jimmy Voegele." runat="server" Skin="Office2007" Font-Bold="true" ForeColor="Red"></telerik:RadLabel>
            <br />
            <br />
            <table>
                <tr >
                    <td style='padding-left:50px'>
                        <telerik:RadLabel Text="End of screening Call:" runat="server"></telerik:RadLabel>
                    </td>
                    <td>
                        <telerik:RadLabel ID="lblEOSK4" Text="" runat="server" style='padding-left:190px'></telerik:RadLabel>
                    </td>
                </tr>
                <tr>
                    <td style='padding-left:50px'>
                        <telerik:RadLabel Text="End of screening Text:" runat="server"></telerik:RadLabel>
                    </td>
                    <td>
                        <telerik:RadLabel ID="lblEOSTMK4" Text="" runat="server" style='padding-left:190px'></telerik:RadLabel>
                    </td>
                </tr>
            </table>
            <br />
            <br />
            <table>
                <tr >
                    <td style='padding-left:50px'>
                        <telerik:RadLabel Text="End of screening Call (5th Grade):" runat="server"></telerik:RadLabel>
                    </td>
                    <td>
                        <telerik:RadLabel ID="lblEOS5TH" Text="" runat="server" style='padding-left:115px'></telerik:RadLabel>
                    </td>
                </tr>
                <tr>
                    <td style='padding-left:50px'>
                        <telerik:RadLabel Text="End of screening Text (5th Grade):" runat="server"></telerik:RadLabel>
                    </td>
                    <td>
                        <telerik:RadLabel ID="lblEOSTM5TH" Text="" runat="server" style='padding-left:115px'></telerik:RadLabel>
                    </td>
                </tr>
            </table>
            <br />
            <br />
            <table>
                <tr >
                    <td style='padding-left:45px'>
                        <telerik:RadLabel Text="" runat="server"></telerik:RadLabel>
                    </td>
                    <td>
                        <telerik:RadButton ID="btnEOS" runat="server"  Text="Add School to EOS Queue" Skin="Office2007"   OnClick="btnEOS_Click"></telerik:RadButton>
                    </td>
                </tr>
            </table>
            <br />
            <br />
            <telerik:RadLabel ID="RadLabel1" Text="Student Count:" runat="server" style='padding-left:50px'></telerik:RadLabel>
            <telerik:RadLabel ID="lblStudentCount" Text="100" runat="server"></telerik:RadLabel>
            <br />
            <telerik:RadLabel ID="RadLabel9" Text="Duplicate Numbers:" runat="server" style='padding-left:50px'></telerik:RadLabel>
            <telerik:RadLabel ID="lblDuplicateNumbers" Text="100" runat="server"></telerik:RadLabel>
            <br />
            <telerik:RadLabel ID="RadLabel11" Text="Total Dial My Calls Count:" runat="server" style='padding-left:50px'></telerik:RadLabel>
            <telerik:RadLabel ID="lblDialMyCalls" Text="100" runat="server"></telerik:RadLabel>
            <br />
            <br />
            <telerik:RadGrid ID="RadGrid2"  DataSourceID="SqlDataSource2" runat="server" ClientSettings-Resizing-AllowColumnResize="true" Skin="Office2007" OnItemCommand="RadGrid2_ItemCommand">
                <GroupingSettings CollapseAllTooltip="Collapse all groups" />
                <ClientSettings>
                    <Resizing AllowColumnResize="True" />
                </ClientSettings>
                <MasterTableView AutoGenerateColumns="True" CommandItemDisplay="Top">
                    <CommandItemSettings ShowExportToExcelButton="true" ShowRefreshButton="false" ShowCancelChangesButton="false" ShowSaveChangesButton="false" ShowAddNewRecordButton="false" />
                </MasterTableView>

            </telerik:RadGrid>
        </telerik:RadPageView>
        <telerik:RadPageView runat="server" ID="RadPageView3">
            <br />
            <telerik:RadLabel ID="lblSchoolsReminder" Text="Schools:" runat="server" Skin="Office2007" Font-Bold="true"></telerik:RadLabel>
            <telerik:RadComboBox ID="cmbSchoolsReminder" AutoPostBack ="true" runat="server" style='padding-left:50px;padding-right:40px' Width="380px" Text="Allison Elem" OnSelectedIndexChanged="cmbSchoolsReminder_SelectedIndexChanged"></telerik:RadComboBox>
            <telerik:RadLabel ID="lblSchoolsReminderQueued" Text="Queued Schools:" runat="server" Skin="Office2007" Font-Bold="true"></telerik:RadLabel>
            <telerik:RadComboBox ID="cmbSchoolsReminderQueued" AutoPostBack ="true" runat="server" style='padding-left:50px;padding-right:40px' Width="380px" Text=""></telerik:RadComboBox>
            <telerik:RadLabel ID="RadLabel17" Text=" ** If you do not see the school you are looking for please contact Todd Waldron or Jimmy Voegele." runat="server" Skin="Office2007" Font-Bold="true" ForeColor="Red"></telerik:RadLabel>
            <br />
            <br />

            <table>
                <tr >
                    <td style='padding-left:50px'>
                        <telerik:RadLabel Text="End of screening Call Reminder:" runat="server"></telerik:RadLabel>
                    </td>
                    <td>
                        <telerik:RadLabel ID="lblEOSReminder" Text="" runat="server" style='padding-left:190px'></telerik:RadLabel>
                    </td>
                </tr>
                <tr>
                    <td style='padding-left:50px'>
                        <telerik:RadLabel Text="End of screening Text Reminder:" runat="server"></telerik:RadLabel>
                    </td>
                    <td>
                        <telerik:RadLabel ID="lblEOSTMReminder" Text="" runat="server" style='padding-left:190px'></telerik:RadLabel>
                    </td>
                </tr>
            </table>
            <br />
            <br />
            <table>
                <tr >
                    <td style='padding-left:45px'>
                        <telerik:RadLabel Text="" runat="server"></telerik:RadLabel>
                    </td>
                    <td>
                        <telerik:RadButton ID="btnEOSReminder" runat="server"  Text="Add School to EOS Reminder Queue" Skin="Office2007"   OnClick="btnEOSReminder_Click"></telerik:RadButton>
                    </td>
                </tr>
            </table>
            <br />
            <br />
            <telerik:RadLabel ID="RadLabel7" Text="Student Count:" runat="server" style='padding-left:50px'></telerik:RadLabel>
            <telerik:RadLabel ID="lblStudentCountReminder" Text="100" runat="server"></telerik:RadLabel>
            <br />
            <telerik:RadLabel ID="RadLabel10" Text="Duplicate Numbers:" runat="server" style='padding-left:50px'></telerik:RadLabel>
            <telerik:RadLabel ID="lblDuplicateNumbersReminder" Text="100" runat="server"></telerik:RadLabel>
            <br />
            <telerik:RadLabel ID="RadLabel13" Text="Total Dial My Calls Count:" runat="server" style='padding-left:50px'></telerik:RadLabel>
            <telerik:RadLabel ID="lblDialMyCallsReminder" Text="100" runat="server"></telerik:RadLabel>

            <br />
            <br />
            <telerik:RadGrid ID="RadGrid3"  DataSourceID="SqlDataSource3" runat="server" ClientSettings-Resizing-AllowColumnResize="true" Skin="Office2007" OnItemCommand="RadGrid3_ItemCommand">

                <GroupingSettings CollapseAllTooltip="Collapse all groups" />
                <ClientSettings>
                    <Resizing AllowColumnResize="True" />
                </ClientSettings>
                <MasterTableView AutoGenerateColumns="True" CommandItemDisplay="Top">
                    <CommandItemSettings ShowExportToExcelButton="true" ShowRefreshButton="false" ShowCancelChangesButton="false" ShowSaveChangesButton="false" ShowAddNewRecordButton="false" />
                </MasterTableView>

            </telerik:RadGrid>
         </telerik:RadPageView>
        <telerik:RadPageView runat="server" ID="RadPageView4">
            <br />
            <table>
                <tr >
                    <td style='padding-left:50px'>
                        <telerik:RadLabel Text="End of treatment Call:" runat="server"></telerik:RadLabel>
                    </td>
                    <td>
                        <telerik:RadLabel ID="lblEOTK4" Text="" runat="server" style='padding-left:190px'></telerik:RadLabel>
                    </td>
                </tr>
                <tr>
                    <td style='padding-left:50px'>
                        <telerik:RadLabel Text="End of treament Text:" runat="server"></telerik:RadLabel>
                    </td>
                    <td>
                        <telerik:RadLabel ID="lblEOTTMK4" Text="" runat="server" style='padding-left:190px'></telerik:RadLabel>
                    </td>
                </tr>
            </table>
            <br />
            <br />
            <table>
                <tr >
                    <td style='padding-left:50px'>
                        <telerik:RadLabel Text="End of treatment Call (5th Grade):" runat="server"></telerik:RadLabel>
                    </td>
                    <td>
                        <telerik:RadLabel ID="lblEOT5TH" Text="" runat="server" style='padding-left:115px'></telerik:RadLabel>
                    </td>
                </tr>
                <tr>
                    <td style='padding-left:50px'>
                        <telerik:RadLabel Text="End of treatment Text (5th Grade):" runat="server"></telerik:RadLabel>
                    </td>
                    <td>
                        <telerik:RadLabel ID="lblEOTTM5TH" Text="" runat="server" style='padding-left:115px'></telerik:RadLabel>
                    </td>
                </tr>
            </table>
            <br />
            <telerik:RadLabel ID="RadLabel14" Text="Student Count:" runat="server" style='padding-left:50px'></telerik:RadLabel>
            <telerik:RadLabel ID="lblEOTStudentCount" Text="100" runat="server"></telerik:RadLabel>
            <br />
            <telerik:RadLabel ID="RadLabel12" Text="Duplicate Numbers:" runat="server" style='padding-left:50px'></telerik:RadLabel>
            <telerik:RadLabel ID="lblDuplicateNumbersEOT" Text="100" runat="server"></telerik:RadLabel>
            <br />
            <telerik:RadLabel ID="RadLabel16" Text="Total Dial My Calls Count:" runat="server" style='padding-left:50px'></telerik:RadLabel>
            <telerik:RadLabel ID="lblDialMyCallsEOT" Text="100" runat="server"></telerik:RadLabel>
            <br />
            <br />
            <telerik:RadGrid ID="RadGrid4"  DataSourceID="SqlDataSource4" runat="server" ClientSettings-Resizing-AllowColumnResize="true" Skin="Office2007" OnItemCommand="RadGrid4_ItemCommand">

                <GroupingSettings CollapseAllTooltip="Collapse all groups" />
                <ClientSettings>
                    <Resizing AllowColumnResize="True" />
                </ClientSettings>
                <MasterTableView AutoGenerateColumns="True" CommandItemDisplay="Top">
                    <CommandItemSettings ShowExportToExcelButton="true" ShowRefreshButton="false" ShowCancelChangesButton="false" ShowSaveChangesButton="false" ShowAddNewRecordButton="false" />
                </MasterTableView>

            </telerik:RadGrid>
        </telerik:RadPageView>
        <telerik:RadPageView runat="server" ID="RadPageView5">
            <br />
            <table>
                <tr >
                    <td style='padding-left:50px'>
                        <telerik:RadLabel Text="Bad Number:" runat="server"></telerik:RadLabel>
                    </td>
                    <td>
                        <telerik:RadTextBox ID="txtBadNumber" runat="server" width="250px"></telerik:RadTextBox>
                    </td>
                    <td>
                        <telerik:RadButton ID="btnBadNumber" Text="Add Bad Number" runat="server" OnClick="btnBadNumber_Click" ></telerik:RadButton>
                    </td>
                </tr>
            </table>
            <br />
            <table>
                <tr >
                    <td style='padding-left:50px'>
                        <telerik:RadLabel Text="API Key:" runat="server"></telerik:RadLabel>
                    </td>
                    <td>
                        <telerik:RadTextBox ID="txtAdminAPIKey" runat="server" width="250px"></telerik:RadTextBox>
                    </td>
                    <td>
                        <telerik:RadButton ID="btnAdminLoadAPIKey" Text="Load API Key" runat="server" OnClick="btnAdminLoadAPIKey_Click" ></telerik:RadButton>
                    </td>
                </tr>
            </table>
            <br />
            <telerik:RadLabel Text="Calls:" runat="server" style='padding-left:50px'></telerik:RadLabel>
            <br />
            <br />
            <table>
                <tr >
                    <td style='padding-left:50px'>
                        <telerik:RadLabel Text="End of screening Call:" runat="server"></telerik:RadLabel>
                    </td>
                    <td>
                        <telerik:RadCombobox ID="cmbAdminEOSCall" runat="server"  Width="500px"></telerik:RadCombobox>
                    </td>
                </tr>
                <tr >
                    <td style='padding-left:50px'>
                        <telerik:RadLabel Text="End of screening Call (5th Grade):" runat="server"></telerik:RadLabel>
                    </td>
                    <td>
                        <telerik:RadCombobox ID="cmbAdminEOS5thGradeCall" runat="server"  Width="500px"></telerik:RadCombobox>
                    </td>
                </tr>
                                <tr >
                    <td style='padding-left:50px'>
                        <telerik:RadLabel Text="End of screening Call Reminder:" runat="server"></telerik:RadLabel>
                    </td>
                    <td>
                        <telerik:RadCombobox ID="cmbAdminEOSReminderCall" runat="server"  Width="500px" ></telerik:RadCombobox>
                    </td>
                </tr>
                                <tr >
                    <td style='padding-left:50px'>
                        <telerik:RadLabel Text="End of treatment Call:" runat="server"></telerik:RadLabel>
                    </td>
                    <td>
                        <telerik:RadCombobox ID="cmbAdminEOTCall" runat="server" Width="500px" ></telerik:RadCombobox>
                    </td>
                </tr>
                                <tr >
                    <td style='padding-left:50px'>
                        <telerik:RadLabel Text="End of treatment Call (5th Grade):" runat="server"></telerik:RadLabel>
                    </td>
                    <td>
                        <telerik:RadCombobox ID="cmbAdminEOT5thGradeCall" runat="server"  Width="500px" ></telerik:RadCombobox>
                    </td>
                </tr>
            </table>
            <br />
            <br />
            <telerik:RadLabel Text="Texts:" runat="server" style='padding-left:50px'></telerik:RadLabel>
            <br />
            <telerik:RadLabel Text="General Text Message  :" runat="server" style='padding-left:50px'></telerik:RadLabel>
            <telerik:RadTextBox ID="txtAdminGeneralTM" runat="server" Width="1000"></telerik:RadTextBox>
            <br />
            <br />
            <telerik:RadLabel Text="5th Grade Text Message:" runat="server" style='padding-left:50px'></telerik:RadLabel>
            <telerik:RadTextBox ID="txtAdmin5thGradeTM" runat="server" Width="1000"></telerik:RadTextBox>
            <br />
            <telerik:RadLabel Text="Text Keywords:" runat="server" style='padding-left:50px'></telerik:RadLabel>
            <telerik:RadCombobox ID="cmbTextKeywords" runat="server"  Width="100px" ></telerik:RadCombobox>
            <br />
            <br />
            <table>
                <tr>
                    <td style='padding-left:50px'>
                        <telerik:RadButton ID="btnAdminSave" Text="Save" runat="server" Skin="Office2007" OnClick="btnAdminSave_Click"></telerik:RadButton>
                    </td>
                </tr>
            </table>
            
        </telerik:RadPageView>
    </telerik:RadMultiPage>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        SelectCommand="">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        SelectCommand="">
    </asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        SelectCommand="">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        SelectCommand="">
    </asp:SqlDataSource>
</asp:Content>
