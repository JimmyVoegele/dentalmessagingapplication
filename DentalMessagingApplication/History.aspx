﻿<%@ Page Title="History" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="History.aspx.cs" Inherits="DentalMessagingApplication.History" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<asp:Content ID="History" ContentPlaceHolderID="MainContent" runat="server">

    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">

    </telerik:RadAjaxManager>

    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
    <Windows>
        <telerik:RadWindow ID="RadWindowDetails" runat="server">
        </telerik:RadWindow>
    </Windows>
    </telerik:RadWindowManager>

    <h2 style="color:#0091ba">Dental Messaging Application History</h2>
    <br />
    <telerik:RadLabel ID="lblRecording" Text="" runat="server" Skin="Office2007"></telerik:RadLabel>
    <telerik:RadLabel ID="lblRecordingName" Text="" runat="server" Skin="Office2007" Font-Bold="true"></telerik:RadLabel>
    <br />
    <telerik:RadLabel Text="Students:" runat="server" Skin="Office2007"></telerik:RadLabel>
    <telerik:RadLabel ID="lblStudents" Text="" runat="server" Skin="Office2007" Font-Bold="true"></telerik:RadLabel>
    <br />
    <br />
    <telerik:RadGrid ExportSettings-ExportOnlyData="true" ID="RadGridAdmin" runat="server" AllowAutomaticDeletes="False" AllowSorting="True" AllowAutomaticInserts="False" AllowAutomaticUpdates="True" AllowPaging="True" AutoGenerateColumns="False" ClientSettings-Resizing-AllowColumnResize="true" GridLines="Both" PageSize="20" Skin="Office2007" DataSourceID="SqlDataSource1" AllowFilteringByColumn="false" IsFilterItemExpanded="false" OnItemCommand="RadGridAdmin_ItemCommand">
        <MasterTableView PageSize="100" AutoGenerateColumns="False" CommandItemDisplay="Top" DataKeyNames="CallID" EditMode="Batch" HorizontalAlign="NotSet" AllowMultiColumnSorting="true" AllowSorting="true">
            <CommandItemSettings  ShowRefreshButton="false" ShowCancelChangesButton="false" ShowSaveChangesButton="false" ShowAddNewRecordButton="false" />
            <BatchEditingSettings EditType="Cell" />
            <CommandItemSettings ShowExportToExcelButton="true" ShowAddNewRecordButton="false" ShowRefreshButton="false" />
            <Columns>
                <telerik:GridBoundColumn DataField="SchoolName" HeaderText="School Name" HeaderStyle-Width="90px" SortExpression="SchoolName" UniqueName="SchoolName" ReadOnly="true" FilterControlWidth="50px">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="FirstName" HeaderText="First Name" HeaderStyle-Width="90px" SortExpression="FirstName" UniqueName="FirstName" ReadOnly="true" FilterControlWidth="50px">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="LastName" HeaderText="Last Name" HeaderStyle-Width="90px" SortExpression="LastName" UniqueName="LastName" ReadOnly="true" FilterControlWidth="50px">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="PhoneNumber" HeaderText="Phone" HeaderStyle-Width="90px" SortExpression="PhoneNumber" UniqueName="PhoneNumber" ReadOnly="true" FilterControlWidth="50px">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Grade" HeaderText="Grade" HeaderStyle-Width="90px" SortExpression="Grade" UniqueName="Grade" ReadOnly="true" FilterControlWidth="50px">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Status" HeaderText="Status" HeaderStyle-Width="90px" SortExpression="Status" UniqueName="Status" ReadOnly="true" FilterControlWidth="50px">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Duration" HeaderText="Duration" HeaderStyle-Width="90px" SortExpression="Duration" UniqueName="Duration" ReadOnly="true" FilterControlWidth="50px">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Attempts" HeaderText="Attempts" HeaderStyle-Width="90px" SortExpression="Attempts" UniqueName="Attempts" ReadOnly="true" FilterControlWidth="50px">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="successful" HeaderText="Successful" HeaderStyle-Width="90px" SortExpression="Successful" UniqueName="Successful" ReadOnly="true" FilterControlWidth="50px">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="CalledAt" HeaderText="Call Time" HeaderStyle-Width="90px" SortExpression="CalledAt" UniqueName="CalledAt" ReadOnly="true" FilterControlWidth="50px">
                </telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
        <GroupingSettings CollapseAllTooltip="Collapse all groups" />
        <ClientSettings AllowKeyboardNavigation="true">
        </ClientSettings>
    </telerik:RadGrid>

    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        SelectCommand="Select CallID, FirstName, LastName, PhoneNumber, Grade, Status, Duration, Attempts, successful, CalledAt from vwDialMyCallsHistory">
    </asp:SqlDataSource>


</asp:Content>
