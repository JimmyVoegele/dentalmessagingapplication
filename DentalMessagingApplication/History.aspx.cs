﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DentalMessagingApplication
{
    public partial class History : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string strCallID = Request.QueryString["ID"];
            SqlDataSource1.SelectCommand = "Select SchoolName, CallID, FirstName, LastName, PhoneNumber, Grade, Status, Duration, Attempts, successful, CalledAt from vwDialMyCallsHistory where CallID = " + strCallID + " Order by SchoolName, LastName, FirstName";
            if (!IsPostBack)
            {
                lblStudents.Text = Request.QueryString["Students"];
                RadGridAdmin.Rebind();
                lblStudents.Text = RadGridAdmin.MasterTableView.Items.Count.ToString();
            }
            string strMessageType = Request.QueryString["MessageType"];
            if (strMessageType.ToLower() == "text")
            {
                RadGridAdmin.Columns[6].Visible = false;
                RadGridAdmin.Columns[7].Visible = false;
                RadGridAdmin.Columns[9].Visible = false;
                lblRecording.Text = "Text Message :";
                lblRecordingName.Text = Request.QueryString["TextContent"];
            }
            else
            {
                lblRecording.Text = "Call Name :";
                lblRecordingName.Text = Request.QueryString["RecordingName"];
            }

        }

        protected void RadGridAdmin_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {

            RadGridAdmin.ExportSettings.Excel.Format = Telerik.Web.UI.GridExcelExportFormat.Biff;
            RadGridAdmin.ExportSettings.IgnorePaging = true;
            RadGridAdmin.ExportSettings.ExportOnlyData = true;
            RadGridAdmin.ExportSettings.OpenInNewWindow = true;
        }
    }
}